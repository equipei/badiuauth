<?php
$string['pluginname'] = 'Autenticação na Plataforma Badiu.Net ';
$string['description'] = 'Plugin de permite autenticar no Moodle usando o login e senha da Plataforma Badiu.Net';
$string['wstoken'] ='Token de autenticação';
$string['wstoken_desc'] ='Senha de acesso a Plataforma de Serviço';
$string['serviceurl'] ='Endereço do serviço do Plataforma Badiu.Net';
$string['serviceurl_desc'] ='Url da instalação da plataforma Badiu.Net Exemplo: http://www.servico.com.br';

$string['usebadiunetoken'] ='Usar o mesmo token do plugin Badiu.Net';
$string['usebadiunetoken_desc'] ='Essa configuração permite autenticar acesso ao webservice usando o mesmo token do plugin Badiu.Net';

$string['logouturl'] ='Endereço de logout';
$string['logouturl_desc'] ='Trata-se do URL que será executado quando o usuário efetuar logout no sistema';

