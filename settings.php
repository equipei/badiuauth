<?php
defined('MOODLE_INTERNAL') || die;

if ($ADMIN->fulltree) {
	$settings->add(new admin_setting_heading('auth_badiuauth/pluginname', '',
            new lang_string('description', 'auth_badiuauth', '')));
			
  // $settings = new admin_settingpage('auth_badiuauth', get_string('pluginname', 'auth_badiuauth'));
  //$ADMIN->add('localplugins', $settings);
   
    global $CFG;
     global $CFG;
    $filepluginbadiunet=$CFG->dirroot.'/local/badiunet/version.php';
	$filepluginbadiunetformlib=$CFG->dirroot.'/local/badiunet/lib/formutil.php';
        
    if(file_exists($filepluginbadiunet) && file_exists($filepluginbadiunetformlib)){
		require_once("$CFG->dirroot/local/badiunet/lib/formutil.php"); 
		$localbadiuwsflutil= new local_badiunet_formutil();
		$localbadiuwsflutiltokenoptions=$localbadiuwsflutil->token_options();
       
	$settings->add(new admin_setting_configselect('auth_badiuauth/usebadiunetoken',
            get_string('usebadiunetoken', 'auth_badiuauth'), get_string('usebadiunetoken_desc', 'auth_badiuauth'),'defaultoken', $localbadiuwsflutiltokenoptions));
    
    }
     

   $settings->add(new admin_setting_configtext('auth_badiuauth/serviceurl', 
		get_string('serviceurl', 'auth_badiuauth'), get_string('serviceurl_desc', 'auth_badiuauth'), ''));

    $settings->add(new admin_setting_configtext('auth_badiuauth/wstoken', 
		get_string('wstoken', 'auth_badiuauth'), get_string('wstoken_desc', 'auth_badiuauth'), ''));
		
	 $settings->add(new admin_setting_configtext('auth_badiuauth/logouturl', 
		get_string('logouturl', 'auth_badiuauth'), get_string('logouturl_desc', 'auth_badiuauth'), ''));
		
		 // Display locking / mapping of profile fields.
    $authplugin = get_auth_plugin('badiuauth');
    display_auth_lock_options($settings, $authplugin->authtype, $authplugin->userfields,
    get_string('auth_fieldlocks_help', 'auth'), false, false);
}

?> 