<?php

require_once($CFG->libdir.'/authlib.php');
require_once($CFG->dirroot.'/auth/badiuauth/lib/netlib.php');
require_once($CFG->dirroot.'/auth/badiuauth/lib/utildata.php');
require_once($CFG->dirroot.'/auth/badiuauth/lib/util.php');

/**
 * badiuauth authentication plugin.
 */
class auth_plugin_badiuauth extends auth_plugin_base {


 /**
     * The name of the component. Used by the configuration.
     */
    const COMPONENT_NAME = 'auth_badiuauth';
    const LEGACY_COMPONENT_NAME = 'auth/badiuauth';

    /**
     * Constructor.
     */
    public function __construct() {
        $this->authtype = 'badiuauth';
        $config = get_config(self::COMPONENT_NAME);
        $legacyconfig = get_config(self::LEGACY_COMPONENT_NAME);
        $this->config = (object)array_merge((array)$legacyconfig, (array)$config);
    }

    /**
     * Returns true if the username and password work and false if they are
     * wrong or don't exist.
     *
     * @param string $username The username (with system magic quotes)
     * @param string $password The password (with system magic quotes)
     * @return bool Authentication success or failure.
     */
    function user_login($username, $password) {
		$netlib=new auth_badiuauth_netlib();
		$util=new auth_badiuauth_util();
		$utildata=new auth_badiuauth_utildata();
		$param=array();
        $param['username']=$username;
		$param['password']=$password;
		$param['_service']='badiu.system.user.saccount.service';
		$param['_function']='login';
		$param['_token']=$netlib->getToken(); 
		
		$host=$netlib->getUrl();
		
		$response=$util->requestService($host,$param);
        $haserror=$util->isResponseError($response);
		
		if($haserror){ return false;}
		$response=$util->convertData($response, true);	
		
		$message=$utildata->getValueOfArray($response, 'message');
        $status= $utildata->getValueOfArray($response, 'status');
        if($status=='accept' && !empty($message) && $message=='true'){return true;}
          
      
		
       return false;
    }



    /**
     * Returns the user information for 'external' users. In this case the
     * attributes provided by badiunet service
     *
     * @return array $result Associative array of user data
     */
    function get_userinfo($username) {
       $netlib=new auth_badiuauth_netlib();
		$util=new auth_badiuauth_util();
		$utildata=new auth_badiuauth_utildata();
		$param=array();
        $param['username']=$username;
		$param['_service']='badiu.system.user.saccount.service';
		$param['_function']='getInfoByUsername';
		$param['_token']=$netlib->getToken(); 
		
		$host=$netlib->getUrl();
		$response=$util->requestService($host,$param);
        $haserror=$util->isResponseError($response);
		
		if($haserror){ return false;}
		$response=$util->convertData($response, true);	
		
		$datauser=array();
		$message=$utildata->getValueOfArray($response, 'message');
        $status= $utildata->getValueOfArray($response, 'status');
		if($status=='accept' && is_array($message)){
			$datauser['firstname']=$utildata->getValueOfArray($message, 'firstname');
			$datauser['lastname']=$utildata->getValueOfArray($message, 'lastname');
			$datauser['email']=$utildata->getValueOfArray($message, 'email');
			$datauser['city']=$utildata->getValueOfArray($message, 'city');
		}else {return null;}
     
        return $datauser;
		
	  
    }

    /**
     * Returns array containg attribute mappings between Moodle and Shibboleth.
     *
     * @return array
     */
    function get_attributes() {
        $configarray = (array) $this->config;

        $moodleattributes = array();
        foreach ($this->userfields as $field) {
            if (isset($configarray["field_map_$field"])) {
                $moodleattributes[$field] = $configarray["field_map_$field"];
            }
        }
        $moodleattributes['username'] = $configarray["user_attribute"];

        return $moodleattributes;
    }

    function prevent_local_passwords() {
        return true;
    }

    /**
     * Returns true if this authentication plugin is 'internal'.
     *
     * @return bool
     */
    function is_internal() {
        return false;
    }

    /**
     * Returns true if this authentication plugin can change the user's
     * password.
     *
     * @return bool
     */
    function can_change_password() {
        return false;
    }

     /**
     * Hook for login page
     *
     */
    function loginpage_hook() {
        global $SESSION, $CFG;

        // Prevent username from being shown on login page after logout
        $CFG->nolastloggedin = true;

        return;
    }

     /**
     * Hook for logout page
     *
     */
    function logoutpage_hook() {
        global $SESSION, $redirect;
		
		if(isset($this->config->logouturl) && !empty($this->config->logouturl)){
			 $redirect =$this->config->logouturl;
		}
       
    }



    /**
     * Prints a form for configuring this authentication plugin.
     *
     * This function is called from admin/auth.php, and outputs a full page with
     * a form for configuring this plugin.
     *
     * @param array $page An object containing all the data for this page.
     */
    function config_form($config, $err, $user_fields) {
       // include "config.html";
    }

    /**
     * Processes and stores configuration data for this authentication plugin.
     *
     *
     * @param object $config Configuration object
     */
    function process_config($config) {
        global $CFG;


        return true;
    }

    /**
     * Cleans and returns first of potential many values (multi-valued attributes)
     *
     * @param string $string Possibly multi-valued attribute from Shibboleth
     */
    function get_first_string($string) {
     
    }
}

