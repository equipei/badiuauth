<?php

defined('MOODLE_INTERNAL') || die();

$plugin->version   = 2022010700;        // The current plugin version (Date: YYYYMMDDXX)
$plugin->requires  = 2014050800;        // Requires this Moodle version
$plugin->component = 'auth_badiuauth'; // Full name of the plugin (used for diagnostics)

