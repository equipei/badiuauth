<?php
require_once($CFG->dirroot.'/auth/badiuauth/lib/pluginconfig.php'); 
class auth_badiuauth_netlib  {

      /**
     * @var string
     */
    private $token;
	
	 /**
     * @var string
     */
    private $url;
		
    function __construct(){
		$this->initConfig();
	}
	
	public function initConfig() {
		
		//check config token
		$plugin=new auth_badiuauth_pluginconfig('auth_badiuauth'); 
		$usebadiunetoken=$plugin->getValue('usebadiunetoken');
		
		if(!empty($usebadiunetoken)){
			//defautl token
			if($usebadiunetoken=='defaultoken'){
				 $bnetplugin=new auth_badiuauth_pluginconfig('local_badiunet');   
                 $this->token=$bnetplugin->getValue('servicetoken');
				 $this->url=$bnetplugin->getValue('serviceurl');
				 
			}else{
				//appsservice token
				global $CFG;
				$filepluginbadiunetsserverdblib=$CFG->dirroot.'/local/badiunet/app/sserver/dblib.php';
				if(file_exists($filepluginbadiunetsserverdblib)){
					require_once($filepluginbadiunetsserverdblib);
					$dblib = new local_badiunet_app_sserver_dblib();
					$this->token=$dblib->get_comun_by_servicekeyinstance($usebadiunetoken,'servicetoken');
					$this->url=$dblib->get_comun_by_servicekeyinstance($usebadiunetoken,'serviceurl');
				}
			}
		} //end if(!empty($usebadiunetoken))
		//local token
		else{
			$this->token=$plugin->getValue('wstoken');
			$this->url=$plugin->getValue('serviceurl');
        }
		$this->token=$this->clean($this->token);
	}
   public function clean($str) {
            if(!empty($str)){
                $str=trim($str);
                $str= str_replace("\\t",'',$str);
                $str= str_replace(" ",'',$str);
            }
           return $str; 
        }
       public function getToken() {
          return $this->token;
      }

      public function setToken($token) {
          $this->token = $token;
      }
	  
	  public function getUrl() {
		   return $this->url."/system/service/process";
      }

      public function setUrl($url) {
          $this->url = $url;
      }

}
