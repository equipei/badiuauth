<?php 
require_once("$CFG->dirroot/auth/badiuauth/lib/netlib.php");
class auth_badiuauth_util  {
   
   function __construct() {
  
   }


/*
 * $out 0 - ojbect | 1- array | 2 - original text
 */
    public function request($url,$pdata=null,$out = 1) {
       $data = $this->requestService($url,$pdata);
       if($this->isResponseError($data)){return $data;}
       if($out==2){return $data;}
       $outarray=false;
       if($out){$outarray=true;}
        $result = $this->convertData($data, $outarray);
        return $result;
    }
 
   function convertData($data,$outarray=false) {
        $max_int_length = strlen((string) PHP_INT_MAX) - 1;
        $json_without_bigints = preg_replace('/:\s*(-?\d{' . $max_int_length . ',})/', ': "$1"', $data);
        $oresult = json_decode($json_without_bigints, $outarray);
        return $oresult;
    }
public function requestService($url,$param=array()) {
        if(is_array($param)){
           $param=json_encode($param);
        }
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $param);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        $result = curl_exec($ch);
        curl_close($ch);
        return $result;
       
    }
	
   function isResponseError($response) {
        if(!is_array($response)){
             $response=$this->convertData($response, true);
        }
         if(is_array($response)){
               if ( array_key_exists('status',$response) 
                       && array_key_exists('info',$response) 
                       && array_key_exists('message',$response) ){return false;}
                  
           }
         return true;
       }  
           
}
?>
