<?php
require_once("../../config.php");

 $urlgoback=optional_param('_urlgoback',null, PARAM_TEXT);
 $appid=optional_param('_appid',null, PARAM_INT);

 if (isloggedin($USER) && $USER->auth == 'badiuauth') {require_logout();}
 if (isloggedin($USER)) {require_logout();}
 
 $return=null;
 if(!empty($urlgoback)){
	 $return=$urlgoback;
	 if(!empty($appid)){$return.="?_appid=$appid";}
}
 if(!empty($return)){redirect($return);}
